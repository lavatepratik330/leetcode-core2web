import java.util.HashMap;

 class demo {
     int countPairsWithSum(int[] array, int targetSum) {
      
        HashMap<Integer, Integer> frequencyMap = new HashMap<>();

        int count = 0;
        for (int num : array) {
         
            int complement = targetSum - num;

            if (frequencyMap.containsKey(complement)) {
               
                count += frequencyMap.get(complement);
            }
            frequencyMap.put(num, frequencyMap.getOrDefault(num, 0) + 1);
        }

        
        return count;
    }

    public static void main(String[] args) {
        int[] array = {1,5,7,1};
        int targetSum = 6;
	int n= 4;
        demo obj=new demo();
        int result =obj.countPairsWithSum(array, targetSum);

        System.out.println("Number of pairs with sum " + targetSum + ": " + result);
    }
}
