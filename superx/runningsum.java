import java.util.Arrays;
class Solution {
    public int[] runningSum(int[] arr) {
        
        int temp=0;
        for(int i=0;i<arr.length;i++){
              temp=temp+arr[i];
              arr[i]=temp;

        }
        return arr;
        
    }
public static void main(String [] args){
    int arr[]= new int []{2,3,6,8,8,9};

   Solution obj =new Solution();
  int [] retvalue=obj.runningSum(arr);
  System.out.println(Arrays.toString(retvalue));


}}
