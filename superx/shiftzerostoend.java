import java.util.Arrays;
class Solution {
    int [] pushZerosToEnd(int[] arr, int n) {
        int j=0;
        for(int i=0;i<arr.length;i++){
            
            if(arr[i]!=0){
                
                int temp=arr[i];
                arr[i]=arr[j];
                arr[j]=temp;
                j++;
            }
            
        }

	return arr;

    }
public static void main(String [] args){

  int arr[]=new int[]{1,0,5,0,5,0,5,5,0};
  int n=9;
Solution obj=new Solution();
  int [] retvalue=obj.pushZerosToEnd(arr,n);
  System.out.println(Arrays.toString(retvalue));



}}
