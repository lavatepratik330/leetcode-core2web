import java.util.Arrays;
class Solution{
    
    static int [] bubbleSort(int arr[]){
     
        int min=Integer.MIN_VALUE;
        int count=0;
        for(int i=0;i<arr.length;i++){
            
            for(int j=0;j<arr.length-1-count;j++){
                
                if(arr[j]>arr[j+1]){
                    int temp=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=temp;
                    
                }
    
                
            }
            count++;
	}
     return arr;
    
    }
    public static void main(String [] args){
      
          int arr[]=new int[]{10,9,8,7,6,5,4,3,2,1};
	  Solution obj=new Solution();
	  int [] retvalue=obj.bubbleSort(arr);
	  System.out.println(Arrays.toString(retvalue));
     

    }}
