import java.util.ArrayList;
import java.util.Collections;
class Solution{
    //Function to find the leaders in the array.
     ArrayList<Integer> leaders(int arr[]){
        ArrayList <Integer>love= new ArrayList<>();
        int temp=Integer.MIN_VALUE;
        
        for(int i=arr.length-1;i>=0;i--){
            
            
            if(arr[i]>=temp){
                
                love.add(arr[i]);
                
                temp=arr[i];
                
            }
        }  
        Collections.reverse(love);
        
         return love;
    }  

public static void main(String [] args){

 int arr[]=new int[]{16,17,4,3,5,2};

 Solution obj=new Solution();
 ArrayList<Integer> retvalue=obj.leaders(arr);
 System.out.println(retvalue);
 
 }







}
