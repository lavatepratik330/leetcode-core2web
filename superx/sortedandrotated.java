class demo {
    static boolean fun (int[] nums) {
        
        int flag = 0;

        for(int i = 1; i < nums.length; i++){
            if(flag == 0 && nums[i-1]>nums[i]){
                flag = 1;

                if(nums[i]>nums[0]){
                    return false;
                }
                else{
                    continue;
                }
            }
            if(flag == 1 && (nums[i]>nums[0] || nums[i-1] > nums[i])){
                return false;
            }
        }
        return true;
    }

    public static void main(String [] args){
    
     int arr[]=new int[]{3,4,5,1,2};
     demo obj=new demo();
     System.out.println(obj.fun(arr));
    }
    
    }
