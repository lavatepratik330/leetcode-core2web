import java.util.Arrays;
class Solution
{
    public static int [] sort(int arr[])
    {
        int count=0;
        int j=0;
        for(int i=0;i<arr.length;i++){
            
            if(arr[i]==count){
                int temp=arr[i];
                arr[i]=arr[j];
                arr[j]=temp;
                j++;
            }
            if(i==(arr.length-1)){
                i=j-1;
                count++;
            }
        }
	return arr;
    }
public static void main(String [] args){

  int [] arr=new int[]{1,0,2,1,0,1,2,2};

  Solution obj=new Solution ();
  System.out.print(Arrays.toString(obj.sort(arr)));

  System.out.println();

}}
