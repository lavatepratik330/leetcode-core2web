import java.util.Arrays;
class Solution {
    static int[] convertToWave(int n, int[] a) {
        // code here
        for(int i = 0; i<a.length-1;i=i+2){
            
            if(a[i] < a[i+1]){
                
                int temp = a[i];
                a[i] = a[i+1];
                a[i+1]=temp;
            }
	    
        }

	return a;
    }

  public static void main(String [] args){
  
    int arr[]=new int[]{1,2,3,4,5};
    int n=6;
    Solution obj =new Solution();
    System.out.println(Arrays.toString(obj.convertToWave(n,arr)));
  }}
