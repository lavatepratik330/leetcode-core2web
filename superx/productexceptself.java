import java.util.Arrays;
class Solution {
    public int[] productExceptSelf(int[] arr) {
       int arr1[]=new int[arr.length];
        int mul1=1;
        for(int i=0;i<arr.length;i++){
            arr1[i]=mul1;
            mul1=mul1*arr[i];
         }
         int mul2=1;
         for(int i=arr.length-1;i>=0;i--){
             arr1[i]=arr1[i]*mul2;
             mul2=mul2*arr[i];
         }

         return arr1;
    }
public static void main(String [] args){

 int arr[]=new int[]{1,2,3,4};

 Solution obj=new Solution();
 System.out.println(Arrays.toString(obj.productExceptSelf(arr)));




}}
