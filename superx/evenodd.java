import java.util.Arrays;
class Solution {
     int[] reArrange(int[] arr, int N) {
        int n=arr.length;
          int evenindex =0;
          int oddindex=1;
          while(evenindex<n&&oddindex<n){
              
              while(evenindex<n&&arr[evenindex]%2==0){
                  
                  evenindex+=2;
                  
              }
              while(oddindex<n&&arr[oddindex]%2 !=0){
                  
                  oddindex+=2;
                  
              }
              
              if(evenindex<n&&oddindex<n){
                  
                  int temp=arr[evenindex];
                  arr[evenindex]=arr[oddindex];
                  arr[oddindex]=temp;
              }
             }
         return arr;
    }

public static void main(String [] args){
  int arr[]=new int[]{2,1,4,3,6,5,8,7};
  int n=arr.length;
  Solution obj=new Solution();
 int[] retobj=obj.reArrange(arr,n);

  System.out.println(Arrays.toString(retobj));


}


}



