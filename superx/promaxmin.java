class Solution{
    
   
    public static long find_multiplication (int arr[], int arr1[], int n, int m) {
        int temp1=Integer.MIN_VALUE;
        int temp2=Integer.MAX_VALUE;
        for(int i=0;i<arr.length;i++){
            
            if(arr[i]>=temp1){
                temp1=arr[i];
            }
            
        }
        for(int j=0;j<arr1.length;j++){
            
            if(arr1[j]<=temp2){
                
                temp2=arr1[j];
            }
        }
        
      return temp1*temp2;  
    }
    
    

public static void main(String [] args){

int arr[] =new int []{1,3,5,2,6,7};
int arr1[]=new int []{3,5,7,3,8,9};
int n=6;
int m=6;
Solution obj=new Solution();
long retvalue= obj.find_multiplication(arr,arr1,n,m);

System.out.println(retvalue);

}}
