class Solution {

    
    // a: input array
    // n: size of array
    // Function to find equilibrium point in the array.
    public static int equilibriumPoint(long nums[], int n) {

        long[]arr=new long[n];
        arr[0]=nums[0];
        int flag=0;
        for(int i=1;i<n;i++){
            arr[i]=arr[i-1]+nums[i];
        }
        for(int i=0;i<n;i++){
            
            if(i==0&&arr[i]==arr[n-1]){
                
                return 1;
            }
            if(i!=0&&arr[i-1]==arr[n-1]-arr[i]){
                
                flag=1;
                return i+1;
            }
        }
return -1;    }


    public static void main(String [] args){
    
    long arr[] =new long[]{1,3,5,2,2};
    int n=5;
    Solution obj=new Solution();
    int retvalue= obj.equilibriumPoint(arr,n);
    System.out.println(retvalue);
    
    }}
