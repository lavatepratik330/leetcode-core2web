class Solution {

    public static int longest(int arr[],int n)
    {
      int max=arr[0];
      int count=1;
      for(int i=1;i<arr.length;i++){
          
          if(arr[i]>=max){
              
              max=arr[i];
              count++;
          }
          
      }  
      return count;
    }
public static void main(String [] args){
int arr[]=new int []{1,3,2,5,4,7};
int n=6;
Solution obj=new Solution();
int retvalue=obj.longest(arr,n);
System.out.println(retvalue);

}}
