class Solution {
    public int pivotIndex(int[] nums) {
        int[]arr=new int[nums.length];
        int flag=0;
        arr[0]=nums[0];
        for(int i=1;i<arr.length;i++){
            arr[i]=arr[i-1]+nums[i];

        }
        for(int i=0;i<nums.length;i++){
            if(i==0&&arr[i]==arr[nums.length-1]){
                return 0;
            }
            if(i!=0&&arr[i-1]==arr[nums.length-1]-arr[i]){
                flag=1;
                return i;
            }
        }
     return -1;   
    }


 public static void main(String [] args){
   int arr[]=new int[]{1,7,3,6,5,6};
   Solution obj=new Solution();
   int retvalue=obj.pivotIndex( arr);
 System.out.println(retvalue);
 
 }}
