import java.util.Arrays;

class Solution {
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {

        int arr[] = new int[nums1.length];
       
        for(int i = 0 ;i<nums1.length; i++){

            int flag = 0;

            for(int j = 0; j<nums2.length; j++){

                if(nums2[j] == nums1[i]){
                    flag = 1;
                }
                if(flag == 1 && nums2[j] > nums1[i]){
                    arr[i] = nums2[j];
                    flag = 2;
                    break;
                }
            }
            if(flag != 2){
                arr[i] = -1;
            }
        }
        return arr;
    }


public static void main(String[] args){
 int arr[]=new int[]{4,1,2};
 int arr1[]=new int []{1,3,4,2};

 Solution obj=new Solution();

int[] retvalue= obj.nextGreaterElement(arr,arr1);
	System.out.println(Arrays.toString(retvalue));



}



}
