class Solution {
   static int findMaxDiff(int arr[], int n) {
        int maxDiff = Integer.MIN_VALUE;
        int arr1[] = new int[arr.length];
        
        for (int i = 0; i < arr.length; i++) {
            arr1[i] = -1;  // Initialize to an invalid value
            
            for (int j = i; j >= 0; j--) {
                if (arr[j] < arr[i]) {
                    arr1[i] = arr[j];
                    break;
                }
            }
        }

        int arr2[] = new int[arr.length];
        for (int i = arr.length - 1; i >= 0; i--) {
            arr2[i] = -1;  // Initialize to an invalid value
            
            for (int j = i; j < arr.length; j++) {
                if (arr[j] < arr[i]) {
                    arr2[i] = arr[j];
                    break;
                }
            }
        }

        for (int i = 0; i < arr.length; i++) {
            if (arr1[i] != -1 && arr2[i] != -1) {
                maxDiff = Math.max(maxDiff, Math.abs(arr1[i] - arr2[i]));
            }
        }
        
        return maxDiff == Integer.MIN_VALUE ? 0 : maxDiff;  // Return 0 if no valid difference found
    }


    public static void main(String [] args){
        Solution obj=new Solution();
	int arr[]=new int []{2, 4, 8, 7, 7, 9, 3};
         int n=7;
	 System.out.println(obj.findMaxDiff(arr,n));
    
    }
}

