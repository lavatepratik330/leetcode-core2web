import java.util.Arrays;
class demo{
    static int[] zigZag(int arr[]){
         
        
        for(int i = 0 ; i < arr.length-1 ; i++){
            
            if(i%2 == 0){
                
                if(arr[i]>arr[i+1]){
                
                    int temp = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = temp;
                }
            }
            else{
                 if(arr[i]<arr[i+1]){
                
                    int temp = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = temp;
                }
                
            }
        }
	return arr;
    }

    public static void main(String [] args){
    
        int arr[]=new int[]{4,3,7,8,6,2,1};

	demo obj=new demo();
	System.out.println(Arrays.toString(obj.zigZag(arr)));
	
    
    
    }}
