class Solution {
    int findMaximum(int[] arr, int n) {
    int min=Integer.MIN_VALUE;
       for(int i=0;i<arr.length;i++){
           
           if(arr[i]>min){
               min=arr[i];
           }
       }
       return min;
    }
public static void main(String [] args){

  int arr[]=new int []{1,12,11,13,16,14,19,18,15,20};
  int n=10;
  Solution obj=new Solution();
  int retvalue=obj.findMaximum(arr,n);
  System.out.println(retvalue);

}}
