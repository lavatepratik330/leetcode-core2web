class Solution {
    int binarysearch(int arr[], int n, int k) {
        
        int mid =arr.length/2;
        
        if(k<arr[mid]){
            
            for(int i=0;i<mid;i++){
                if(arr[i]==k){
                    
                    return i;
                }
                
            }
        }
       else if(k>arr[mid]){
            
            for(int i=mid+1;i<arr.length;i++){
                if(arr[i]==k){
                     
                     return i;
                }
            }
        }
        
       else if(k==arr[mid]){
            return mid;
        }
        
        return -1;
    
    }
public static void main(String [] args){
int arr[]=new int []{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
int n=11;
int k=6;
Solution obj=new Solution();

int retvalue=obj.binarysearch(arr,n,k);
System.out.println(retvalue);

}}
