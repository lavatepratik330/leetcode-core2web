import java.util.Arrays;
class Solution
{
    public int[][] transpose(int n,int arr[][])
    {
      int arr1[][]=new int[arr.length][arr.length];
      for(int i=0;i<arr.length;i++){
          
          for(int j=0;j<arr.length;j++){
              
              arr1[i][j]=arr[j][i];
          }
          
      } 
      for(int i=0;i<arr.length;i++){
          
          for(int j=0;j<arr.length;j++){
              
              arr[i][j]=arr1[i][j];
          }
          
      }

      return arr;
    }
public static void main(String [] args){

  int arr[][]=new int[][]{{1,1,1,1},{2,2,2,2},{3,3,3,3},{4,4,4,4}};
  Solution obj=new Solution();
  int n=arr.length;
  int[][] retvalue=obj.transpose(n,arr);
  System.out.println(Arrays.deepToString(retvalue));



}}
