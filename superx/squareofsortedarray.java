import java.util.Arrays;
class Solution {
    public int[] sortedSquares(int[] arr) {
        int nums[]=new int[arr.length];

        for(int i=0;i<arr.length;i++){
            nums[i]=arr[i]*arr[i];
        }

        Arrays.sort(nums);
        return nums;
    }
public static void main(String [] args){

   int arr[]=new int[]{2,4,1,5,6,0};
   Solution obj=new Solution();
   int [] retvalue=obj.sortedSquares(arr);
   System.out.println(Arrays.toString(retvalue));
}}
