import java.util.List;
import java.util.Arrays;
import java.util.HashSet;
import java.util.ArrayList;
class Solution {
    @SuppressWarnings("unchecked")
	public int[] intersection(int[] nums1, int[] nums2) {
        
        HashSet set =new HashSet();
        List <Integer> I = new ArrayList<Integer>();
        for(int element : nums1){
            set.add(element);
        }
        for(int i=0;i<nums2.length;i++){
            if(set.contains(nums2[i])){
                I.add(nums2[i]);
                set.remove(nums2[i]);
            }
        }
        return I.stream().mapToInt(Integer::intValue).toArray();
   }

   public static void main(String [] args){
   
       int arr1[]=new int []{1,2,2,1};
       int arr2[]=new int []{2,2};
      Solution obj=new Solution();
      int [] retvalue=obj.intersection(arr1,arr2);
      System.out.println(Arrays.toString(retvalue));
   
   
   }}
