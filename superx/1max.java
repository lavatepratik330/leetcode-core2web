import java.util.Arrays;
class Solution {
    public int[] rowAndMaximumOnes(int[][] arr) {
        int arr1[]= new int[2];
        int  maxsum=0;
        for(int i=0;i<arr.length;i++){
              int sum=0;
            for(int j=0;j<arr[i].length;j++){
                  if(arr[i][j]==1){
                      sum=sum+1;
                  }
                 }

                  if(sum>maxsum){
                     arr1[0]=i;
                     arr1[1]=sum;
                     maxsum=sum; 
                    
                  }
        }
        return arr1;
    
}

public static void main(String [] args){

  int arr[][]=new int [][]{{1,0,1,0},{0,1,0,1,0,1}};
  Solution obj=new Solution();
   System.out.println(Arrays.toString(obj.rowAndMaximumOnes(arr)));
   System.out.println();

}}
