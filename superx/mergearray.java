import java.util.Arrays; 
class demo {
    public int[] merge(int[] nums1, int m, int[] nums2, int n) {
        int index1=0;
        int index2=0;
        int in=0;

        int []arr=new int[m+n];
        if(n==0){
            return nums1;
        }
        while(index1<m&&index2<n){
            if(nums1[index1]<nums2[index2]){
                arr[in]=nums1[index1];
                index1++;
            }
            else{
                arr[in]=nums2[index2];
                index2++;
            }
            in++;
        }
        while(index2<n){
            arr[in]=nums2[index2];
            index2++;
            in++;

        }
        while(index1<m){
            arr[in]=nums1[index1];
            index1++;
            in++;
        }
        for(int i=0;i<m+n;i++){
            nums1[i]=arr[i];
        }
	return nums1;

        
    }
public static void main(String[] args){

int arr1[]=new int[]{1,2,3,0,0,0};
int arr2[]=new int[]{2,5,6};
int m=6;
int n=3;
demo obj=new demo();
int[] retvalue =obj.merge(arr1,m,arr2,n );
System.out.println(Arrays.toString(retvalue));

}
 }
